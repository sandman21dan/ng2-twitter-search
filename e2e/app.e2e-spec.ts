import { NgxTwitterSearchPage } from './app.po';

describe('ngx-twitter-search App', () => {
  let page: NgxTwitterSearchPage;

  beforeEach(() => {
    page = new NgxTwitterSearchPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

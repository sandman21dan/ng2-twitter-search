import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Category } from '../app.component';
import { Status } from '../services/twitter/twitter.interface';

@Component({
  selector: 'tweet-card',
  templateUrl: './tweet-card.component.html',
  styleUrls: ['./tweet-card.component.scss']
})
export class TweetCardComponent implements OnInit {
  @Input() status: Status;
  @Input() categories: Category[];
  @Output() onCategorySelected = new EventEmitter<Category>();

  constructor() { }

  ngOnInit() {
    this.status.user.screen_name
  }

  selectCategory(category: Category) {
    this.onCategorySelected.emit(category);
  }
}

/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, EventEmitter } from '@angular/core';

import { TweetCardComponent } from './tweet-card.component';
import { TwitterSearchResponse, Status } from '../services/twitter/twitter.interface';

describe('TweetCardComponent', () => {
  let component: TweetCardComponent;
  let fixture: ComponentFixture<TweetCardComponent>;
  const mockTweet: Status = (<TwitterSearchResponse>require('../services/twitter/twitter-search.mock.json')).statuses[3];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TweetCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TweetCardComponent);
    component = fixture.componentInstance;
    component.status = mockTweet;
    component.categories = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should emit event with selected category', () => {
    spyOn(component.onCategorySelected, 'emit');
    component.categories.push({ name: 'Test', statuses: [] });

    component.selectCategory(component.categories[0]);
    expect(component.onCategorySelected.emit).toHaveBeenCalledWith(component.categories[0]);
  });
});

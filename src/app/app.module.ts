import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// components
import { AppComponent } from './app.component';

// services
import { TwitterService } from './services/twitter/twitter.service';
import { TweetCardComponent } from './tweet-card/tweet-card.component';

@NgModule({
  declarations: [
    AppComponent,
    TweetCardComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
  ],
  providers: [
    TwitterService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

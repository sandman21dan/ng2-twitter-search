/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { ReactiveFormsModule, FormControl, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Observable } from 'rxjs';

import { TwitterService } from './services/twitter/twitter.service';
import { TwitterSearchResponse } from './services/twitter/twitter.interface';
import { AppComponent, Category } from './app.component';
import { TweetCardComponent } from './tweet-card/tweet-card.component';

describe('AppComponent', () => {
  let component: AppComponent;
  let twitterService: TwitterService;

  const mockTwitterResponse: TwitterSearchResponse = require('./services/twitter/twitter-search.mock.json');

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TwitterService,
        AppComponent,
      ],
      declarations: [
        AppComponent,
        TweetCardComponent
      ],
      imports: [
        HttpModule,
        ReactiveFormsModule,
        FormsModule,
      ]
    });
    component = TestBed.get(AppComponent);
    twitterService = TestBed.get(TwitterService);

    // mock twitter service
    spyOn(twitterService, 'search').and.returnValue(Observable.from([mockTwitterResponse]));

    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('Should search twitter and remove any filters', () => {
    spyOn(component, 'removeCategoryFilters');
    const mockSearch = 'mock twitter search';

    component.search(mockSearch);
    expect(twitterService.search).toHaveBeenCalledWith(mockSearch);
    expect(component.tweets).toEqual(mockTwitterResponse.statuses);
    expect(component.feed).toEqual(mockTwitterResponse.statuses);
    expect(component.removeCategoryFilters).toHaveBeenCalled();
  });

  it('Should search more results and append to current feed', () => {
    component.search('test search');
    expect(component.tweets.length).toEqual(15);
    spyOn(twitterService, 'searchMoreResults').and.returnValue(Observable.from([mockTwitterResponse]));
    component.moreResults();

    expect(twitterService.searchMoreResults).toHaveBeenCalled();
    expect(component.tweets.length).toEqual(30);
    expect(component.feed.length).toEqual(30);
  });

  it('Should store a list of categories', () => {
    expect(component.categories).toEqual([]);
  });

  it('Should add categories and stop editing mode', () => {
    component.isAddCategory = true;
    component.newCategoryName = 'News';
    component.addCategory('News');
    expect(component.categories[0]).toEqual({ name: 'News', statuses: [] });
    component.addCategory('Events');
    expect(component.categories).toEqual([
      { name: 'News', statuses: [] },
      { name: 'Events', statuses: [] }
    ]);
    expect(component.isAddCategory).toBeFalsy();
    expect(component.newCategoryName).toEqual('');
  });

  it('Should not add a category with the same name', () => {
    component.addCategory('News');
    component.addCategory('News');

    expect(component.categories).toEqual([
      { name: 'News', statuses: [] }
    ]);
  });

  it('Should remove categories', () => {
    component.addCategory('News');
    component.categories[0].active = true;
    component.addCategory('Stories');
    component.addCategory('Events');

    component.removeActiveCategory();
    expect(component.categories).toEqual([
      { name: 'Stories', statuses: [], active: false },
      { name: 'Events', statuses: [], active: false }
    ]);
  });

  it('Should add a tweet into a category', () => {
    component.addCategory('News');
    component.addStatusIntoCategory(component.categories[0], component.feed[1]);

    expect(component.categories[0].statuses[0]).toEqual(component.feed[1]);
  });

  it('Should select a category and disable others', () => {
    component.addCategory('News');
    component.addCategory('Stories');
    // activate second category
    component.categories[1].active = true;

    component.selectCategory(component.categories[0]);
    expect(component.feed).toBe(component.categories[0].statuses);
    expect(component.categories[0].active).toBeTruthy();
    expect(component.categories[1].active).toBeFalsy();
  });

  it('Should disable all category filters', () => {
    component.addCategory('News');
    component.addCategory('Stories');
    component.selectCategory(component.categories[0]);

    component.removeCategoryFilters();
    expect(component.categories[0].active).toBeFalsy();
    expect(component.feed).toBe(component.tweets);
  });

  // it(`should have as title 'app works!'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('app works!');
  // }));

  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('app works!');
  // }));
});

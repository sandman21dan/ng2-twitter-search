import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { TwitterService } from './services/twitter/twitter.service';
import { TwitterSearchResponse, Status } from './services/twitter/twitter.interface';

export interface Category {
  name: string;
  statuses: Status[];
  active?: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public searchControl = new FormControl();
  public tweets: Status[] = [];
  public feed: Status[] = [];
  public categories: Category[] = [];
  public newCategoryName: string = '';
  public isAddCategory: boolean = false;

  constructor(private twitter: TwitterService) {
  }

  ngOnInit() {
    this.startSearch();
  }

  startSearch() {
    this.searchControl.valueChanges
      .debounceTime(600)
      .distinctUntilChanged()
      .subscribe( search => {
        this.search(search);
      });
  }

  search(query: string) {
    this.twitter.search(query).subscribe( resp => {
      this.tweets = resp.statuses;
      this.feed = this.tweets;
      this.removeCategoryFilters();
    });
  }

  moreResults() {
    this.twitter.searchMoreResults().subscribe( resp => {
      this.tweets = this.tweets.concat(resp.statuses);
      this.feed = this.tweets;
    });
  }

  addCategory(name: string) {
    let shouldAdd = true;

    this.categories.forEach( item => {
      if (item.name === name) {
        shouldAdd = false;
      }
    });
    
    if (shouldAdd === true) {
      this.categories.push({
        name,
        statuses: []
      });
      this.isAddCategory = false;
      this.newCategoryName = '';
    }
  }

  removeActiveCategory() {
    this.categories = this.categories.filter( item => {
      return item.active !== true;
    });
    this.removeCategoryFilters();
  }

  selectCategory(category: Category) {
    this.categories.map( item => {
      item.active = false;
    });

    category.active = true;
    this.feed = category.statuses;
  }

  addStatusIntoCategory(category: Category, status: Status) {
    this.categories.forEach( item => {
      if (item.name === category.name) {
        item.statuses.push(status);
      }
    });
  }

  isCategoryActive() {
    let result = false;
    this.categories.forEach( item => {
      if (item.active === true) {
        result = true;
      }
    });

    return result;
  }

  removeCategoryFilters() {
    this.categories.map( item => {
      item.active = false;
    });

    this.feed = this.tweets;
  }
}

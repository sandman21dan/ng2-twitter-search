/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Http, HttpModule, BaseRequestOptions, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Observable } from 'rxjs';

import { TwitterService } from './twitter.service';
import { TwitterSearchResponse } from './twitter.interface';

describe('TwitterServiceService', () => {
  let service: TwitterService;
  let http: Http;
  let mockBackend: MockBackend;

  const mockSearch = 'This is a twitter search';
  const mockTwitterSearchResponse: TwitterSearchResponse = require('./twitter-search.mock.json'); 

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule ],
      providers: [
        TwitterService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
            return new Http(backend, options);
          },
          deps: [ MockBackend, BaseRequestOptions ]
        }
      ]
    });
    service = TestBed.get(TwitterService);
    http = TestBed.get(Http);
    mockBackend = TestBed.get(MockBackend);
  });

  it('Should ...', () => {
    expect(service).toBeTruthy();
  });

  it('Should call http get when searching', () => {
    spyOn(http, 'get').and.returnValue(Observable.from([mockTwitterSearchResponse]));
    service.search(mockSearch);

    expect(http.get).toHaveBeenCalledWith('/api?q=' + mockSearch);
  });

  it('Should return an Observable with tweets as result from search', (done) => {
    // mock the http response
    mockBackend.connections.subscribe( connection => {
      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(mockTwitterSearchResponse)
      })));
    });

    service.search(mockSearch).subscribe( resp => {
      expect(resp).toEqual(mockTwitterSearchResponse);
      expect(resp.statuses.length).toEqual(15);
      done();
    });
  });

  it('Should store url for more results', (done) => {
    // mock the http response
    mockBackend.connections.subscribe( connection => {
      connection.mockRespond(new Response(new ResponseOptions({
        body: JSON.stringify(mockTwitterSearchResponse)
      })));
    });

    service.search(mockSearch).subscribe( resp => {
      expect(resp).toEqual(mockTwitterSearchResponse);
      expect(service.nextResults).toEqual(mockTwitterSearchResponse.search_metadata.next_results);
      done();
    });
  });

  it('Should get more results for last search', () => {
    service.nextResults = mockTwitterSearchResponse.search_metadata.next_results;
    spyOn(http, 'get').and.returnValue(Observable.from([mockTwitterSearchResponse]));
    service.searchMoreResults();

    expect(http.get).toHaveBeenCalledWith('/api' + mockTwitterSearchResponse.search_metadata.next_results);
  });
});

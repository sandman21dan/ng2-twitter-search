import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { TwitterSearchResponse } from './twitter.interface';
import 'rxjs/add/operator/map';

@Injectable()
export class TwitterService {
  private nextResults: string;

  constructor(private http: Http) { }

  search(query: string): Observable<TwitterSearchResponse> {
    return this.http.get('/api?q=' + query)
      .map( resp => {
        let result: TwitterSearchResponse = resp.json();
        this.nextResults = result.search_metadata.next_results;
        return result
      });
  }

  searchMoreResults() {
    return this.http.get('/api' + this.nextResults)
      .map( resp => {
        let result: TwitterSearchResponse = resp.json();
        this.nextResults = result.search_metadata.next_results;
        return result
      });
  }
}
